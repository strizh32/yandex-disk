import './promise-polyfill'
import 'babel-polyfill'
import { app } from './app'

app.$mount('#app');

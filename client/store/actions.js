import axios from 'axios';
import {downloadURI} from '../inc/functions';

const refreshList = (state, value) => {
  return new Promise((resolve, reject) => {
    axios.get(state.yandexDiskApi.baseUrl, {
      params: {
        path: value.path
      },
      headers: {
        'Authorization': 'OAuth ' + state.oAuth.accessToken
      }
    })
    .then(response => {
      try {
        state.items = response.data._embedded.items;
        resolve();
      } catch (e) {
        console.error(e);
      }
    })
    .catch(error => {
      console.log(error);
      reject();
    });
  });
};

export default {
  REFRESH_LIST({state, commit}, value) {
    return refreshList(state, value).then(() => {
      commit('UPDATE_CRUMBS', state.route);
    });
  },

  ADD_RESOURCE({state, dispatch, commit}, value) {
    if (!value) return;
    value = value.replace('/', '');

    return new Promise((resolve, reject) => {
      if (state.route.path === '/') {
        value = '/' + value;
      } else {
        value = state.route.path + '/' + value;
      }

      value = encodeURIComponent(value);

      axios.put(state.yandexDiskApi.baseUrl + '?path=' + value, null, {
        headers: {
          'Authorization': 'OAuth ' + state.oAuth.accessToken
        }
      })
      .then(response => {
        if (response.status === 201) {
          dispatch('REFRESH_LIST', state.route);
          commit('CLEAR_NEW_FOLDER_NAME');
          resolve();
        }
      })
      .catch(error => {
        console.error(error);
        reject();
      });
    });
  },

  DOWNLOAD_RESOURCE({state}, value) {
    return new Promise((resolve, reject) => {
      axios.get(state.yandexDiskApi.baseUrl + '/download', {
        params: {
          path: value.path
        },
        headers: {
          'Authorization': 'OAuth ' + state.oAuth.accessToken
        }
      })
      .then(response => {
        if (response.status === 200) {
          console.log(response);
          let href = response.data.href;
          downloadURI(href);
          resolve();
        }
      })
      .catch(error => console.error(error));
    });
  },

  DELETE_RESOURCE({state}, value) {
    return new Promise((resolve, reject) => {
      if (confirm('Вы хотите удалить этот ресурс. Продолжить?')) {
        axios.delete(state.yandexDiskApi.baseUrl, {
          params: {
            path: value.path
          },
          headers: {
            'Authorization': 'OAuth ' + state.oAuth.accessToken
          }
        })
        .then(response => {
          if (response.status === 204 || response.status === 202) {
            resolve();
          }
        })
        .catch(error => {
          console.error(error);
          reject();
        });
      }
    });
  },
};
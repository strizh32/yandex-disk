import getInitialState from './initialState';
import breadcrumb from 'url-breadcrumb';

export const state = getInitialState();

export const mutations = {
  UPDATE_ACCESS_TOKEN(state, value) {
    state.oAuth.accessToken = value;
  },

  UPDATE_CRUMBS(state, value) {
    state.crumbs = breadcrumb(value.path, {home: 'Диск'});
  },

  SET_NEW_FOLDER_NAME(state, value) {
    state.newFolderName = value;
  },

  CLEAR_NEW_FOLDER_NAME(state) {
    state.newFolderName = '';
  },

  //Привести данные к начальному состоянию
  RESET_STATE(state) {
    Object.assign(state, getInitialState());
  }
};

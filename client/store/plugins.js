import createPersist from 'vuex-localstorage';

// Синхронизирует store => localStorage
const syncVuexAndLocalStorage = createPersist({
  namespace: 'yandex_disk_explorer',
  initialState: {}
});

export default [
  syncVuexAndLocalStorage
];

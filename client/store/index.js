import Vue from 'vue';
import Vuex from 'vuex';
import {state, mutations} from './mutations'
import actions from './actions'
import plugins from './plugins'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  mutations,
  actions,
  plugins
})

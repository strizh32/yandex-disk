export default function () {

  return {
    oAuth: {
      appId: '7358eea272734cd0a4a0973ceaff8fa3',
      accessToken: null,
      url: 'https://oauth.yandex.ru/authorize'
    },
    yandexDiskApi: {
      baseUrl: 'https://cloud-api.yandex.net/v1/disk/resources'
    },
    items: [],
    crumbs: [],
    newFolderName: ''
  }
};
